import HomePage from './pages/home.vue';
import SwiperPage from './pages/swiper.vue';
import CompennetsdemoPage from './pages/compennetsdemo.vue';
import FormPage from './pages/form.vue';
import DynamicRoutePage from './pages/dynamic-route.vue';
import NotFoundPage from './pages/not-found.vue';

import PanelLeftPage from './pages/panel-left.vue';
import PanelRightPage from './pages/panel-right.vue';

import Page1 from './pages/page1.vue';
import Page2 from './pages/page2.vue';
import Page3 from './pages/page3.vue';
import Page5 from './pages/page5.vue';

export default [{
    path: '/',
    component: Page1,
  },
  {
    path: '/page2/',
    component: Page2,
  },
  {
    path: '/page3/',
    component: Page3,
  },
  {
    path: '/page5/',
    component: Page5,
  },
  {
    path: '/home/',
    component: HomePage,
  },
  {
    path: '/panel-left/',
    component: PanelLeftPage,
  },
  {
    path: '/panel-right/',
    component: PanelRightPage,
  },
  {
    path: '/swiper/',
    component: SwiperPage,
  },
  {
    path: '/compennetsdemo/',
    component: CompennetsdemoPage,
  },
  {
    path: '/form/',
    component: FormPage,
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage,
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];
